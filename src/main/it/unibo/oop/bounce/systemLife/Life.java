package it.unibo.oop.bounce.systemLife;

public interface Life {
	
	void extraLife();
	
	void lessLife();
	
	int getLives();

}
