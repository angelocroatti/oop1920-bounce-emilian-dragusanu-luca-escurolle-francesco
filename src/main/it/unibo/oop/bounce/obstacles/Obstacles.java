package it.unibo.oop.bounce.obstacles;

import it.unibo.oop.bounce.ball.Ball;


public interface Obstacles {

void onCollide(Ball ball);

}
