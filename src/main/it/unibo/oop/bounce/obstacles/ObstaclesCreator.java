package it.unibo.oop.bounce.obstacles;

public interface ObstaclesCreator {
	
	void createObstacles();

}
